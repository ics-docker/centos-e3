FROM registry.esss.lu.se/ics-docker/centos-ics:latest

LABEL maintainer "simon.rose@ess.eu"

# Add ESS repos to provide Ethercat support packages
#
# This process uses the standard yum repository configuration command, followed
# by additional repo configuration lines that are required to set up the GPG
# configuration for the repositories. Refer to
# https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-repository/-/blob/master/vars/RedHat.yml
# for repository configuration information. 
# Alternatively, refer to the following files on any ESS-configured host:
# * /etc/yum.repos.d/ESS-ICS.repo 
# * /etc/yum.repos.d/ESS-EPEL.repo
#

ARG MINIFORGE_VERSION=22.11.1-4

RUN yum-config-manager --add-repo=https://artifactory.esss.lu.se/artifactory/rpm-ics/centos/7/x86_64/

RUN echo "gpgcheck = 0" >> /etc/yum.repos.d/artifactory.esss.lu.se_artifactory_rpm-ics_centos_7_x86_64_.repo \
  && echo "gpgkey = https://artifactory.esss.lu.se/artifactory/rpm-ics/RPM-GPG-KEY-ICS" >> /etc/yum.repos.d/artifactory.esss.lu.se_artifactory_rpm-ics_centos_7_x86_64_.repo \
  && echo "repo_gpgcheck = 1" >> /etc/yum.repos.d/artifactory.esss.lu.se_artifactory_rpm-ics_centos_7_x86_64_.repo

RUN yum-config-manager --add-repo=https://artifactory.esss.lu.se/artifactory/epel-mirror/7/x86_64/

RUN echo "gpgcheck = 1" >> /etc/yum.repos.d/artifactory.esss.lu.se_artifactory_epel-mirror_7_x86_64_.repo \
  && echo "gpgkey = https://artifactory.esss.lu.se/artifactory/epel-mirror/RPM-GPG-KEY-EPEL-7" >> /etc/yum.repos.d/artifactory.esss.lu.se_artifactory_epel-mirror_7_x86_64_.repo \
  && echo "repo_gpgcheck = 0" >> /etc/yum.repos.d/artifactory.esss.lu.se_artifactory_epel-mirror_7_x86_64_.repo

RUN yum update -y \
  # Use IUS repository to install recent version of git (git2u)
  && yum -y install  https://repo.ius.io/ius-release-el7.rpm \
  && yum install -y \
    file \
    patch \
    tree \
    vim \
    nano \
    m4 \
    gcc-c++ \
    git236 \
    expectk \
    tclx \
    graphviz \
    libtirpc-devel \
    re2c \
    readline-devel \
    hdf5-devel \
    libxml2-devel \
    libjpeg-turbo-devel \
    libtiff-devel \
    blosc-devel \
    netcdf-devel \
    opencv-devel \
    wget \
    sudo \
    python-devel \
    python3-devel \
    patchelf \
    boost-devel \
    glib2-devel \
    libXt-devel \
    libXp-devel \
    libXmu-devel \
    libXpm-devel \
    libXp-devel \
    libpng12-devel \
    libzip-devel \
    libusb-devel \
    libusbx-devel \
    libudev-devel \
    gsl-devel \
    unzip \
    libtool \
    popt-devel \
    net-snmp-devel \
    faketime \
    libraw1394 \
    bzip2-devel \
    freetype-devel \
    lcms2-devel \
    ethercat-generic-dkms-1.5.2.ESS1-1 \
    perl-Test-Simple \
    libevent-devel \
    gdb \
    libasan \
    qt5-qttools-devel \
    cmake3 \
    pugixml-devel \
  && yum clean all

# Install Mambaforge
RUN curl -OL https://github.com/conda-forge/miniforge/releases/download/${MINIFORGE_VERSION}/Mambaforge-Linux-x86_64.sh \
  && chmod +x Mambaforge-Linux-x86_64.sh \
  && ./Mambaforge-Linux-x86_64.sh -b -p /opt/conda \
  && ln -s /opt/conda/etc/profile.d/mamba.sh /etc/profile.d/mamba.sh \
  && ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh \
  && /opt/conda/bin/conda config --system --set channel_alias https://artifactory.esss.lu.se/artifactory/api/conda \
  && /opt/conda/bin/conda config --system --add channels conda-e3-virtual \
  && rm Mambaforge-Linux-x86_64.sh

#
# This docker image is used to build old and new e3 releases so it's mandatory
# to have the older and newer versions of SDKs.
#
# cross-compiler version
ENV ESS_LINUX_VERSION_1=1.0.0 \
    ESS_LINUX_KERNEL_1=5.4 \
    ESS_LINUX_SHORT_SHA_1=81904dc2

# Install ifc14xx toolchain
ENV IFC14XX_TOOLCHAIN_SCRIPT_1 ifc14xx-glibc-x86_64-ifc14xx-toolchain-ppc64e6500-ifc14xx-64b-toolchain-${ESS_LINUX_VERSION_1}-${ESS_LINUX_KERNEL_1}-${ESS_LINUX_SHORT_SHA_1}.sh
RUN wget --quiet -P /tmp https://artifactory.esss.lu.se/artifactory/yocto/toolchain/v${ESS_LINUX_VERSION_1}/${IFC14XX_TOOLCHAIN_SCRIPT_1} \
  && chmod a+x /tmp/${IFC14XX_TOOLCHAIN_SCRIPT_1} \
  && /tmp/${IFC14XX_TOOLCHAIN_SCRIPT_1} -y \
  && rm -f /tmp/${IFC14XX_TOOLCHAIN_SCRIPT_1}

# Install cct toolchain
ENV CCT_TOOLCHAIN_SCRIPT_1 cct-glibc-x86_64-cct-toolchain-corei7-64-cct-64-toolchain-${ESS_LINUX_VERSION_1}-${ESS_LINUX_KERNEL_1}-${ESS_LINUX_SHORT_SHA_1}.sh
RUN wget --quiet -P /tmp https://artifactory.esss.lu.se/artifactory/yocto/toolchain/v${ESS_LINUX_VERSION_1}/${CCT_TOOLCHAIN_SCRIPT_1} \
  && chmod a+x /tmp/${CCT_TOOLCHAIN_SCRIPT_1} \
  && /tmp/${CCT_TOOLCHAIN_SCRIPT_1} -y \
  && rm -f /tmp/${CCT_TOOLCHAIN_SCRIPT_1}

# cross-compiler version
ENV ESS_LINUX_VERSION_2=1.6.0 \
    ESS_LINUX_KERNEL_2=5.4 \
    ESS_LINUX_SHORT_SHA_2=9abb12d1

# Install ifc14xx toolchain
ENV IFC14XX_TOOLCHAIN_SCRIPT_2 ifc14xx-glibc-x86_64-ifc14xx-toolchain-ppc64e6500-ifc14xx-64b-toolchain-${ESS_LINUX_VERSION_2}-${ESS_LINUX_KERNEL_2}-${ESS_LINUX_SHORT_SHA_2}.sh
RUN wget --quiet -P /tmp https://artifactory.esss.lu.se/artifactory/yocto/toolchain/v${ESS_LINUX_VERSION_2}/${IFC14XX_TOOLCHAIN_SCRIPT_2} \
  && chmod a+x /tmp/${IFC14XX_TOOLCHAIN_SCRIPT_2} \
  && /tmp/${IFC14XX_TOOLCHAIN_SCRIPT_2} -y \
  && rm -f /tmp/${IFC14XX_TOOLCHAIN_SCRIPT_2}

# Install cct toolchain
ENV CCT_TOOLCHAIN_SCRIPT_2 cct-glibc-x86_64-cct-toolchain-corei7-64-cct-64-toolchain-${ESS_LINUX_VERSION_2}-${ESS_LINUX_KERNEL_2}-${ESS_LINUX_SHORT_SHA_2}.sh
RUN wget --quiet -P /tmp https://artifactory.esss.lu.se/artifactory/yocto/toolchain/v${ESS_LINUX_VERSION_2}/${CCT_TOOLCHAIN_SCRIPT_2} \
  && chmod a+x /tmp/${CCT_TOOLCHAIN_SCRIPT_2} \
  && /tmp/${CCT_TOOLCHAIN_SCRIPT_2} -y \
  && rm -f /tmp/${CCT_TOOLCHAIN_SCRIPT_2}

# Install google git repo
RUN wget -O /usr/local/bin/repo https://storage.googleapis.com/git-repo-downloads/repo \
  && chmod a+x /usr/local/bin/repo

# Install python packages
RUN python3 -m pip install --no-cache-dir --upgrade pip && \
    python3 -m pip install --no-cache-dir pyyaml && \
    python3 -m pip install --no-cache-dir e3 -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple && \
    python3 -m pip install --no-cache-dir run-iocsh -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple && \
    python3 -m pip install --no-cache-dir pytest && \
    python3 -m pip install --no-cache-dir pyepics==3.5.0 && \
    python3 -m pip install --no-cache-dir opcua && \
    python3 -m pip install --no-cache-dir pymodbus


# Add user that will be used by gitlab-ci to compile
# This user shall have the proper uid to have write access on E3 NFS share
# It shall match the uid and gid of the user defined in LDAP
# If the image is intended to be used for local builds then it's possible
# to build it and use your own username, user Id, etc from your linux box.
# This helps out to avoid issues with write/read permission when mounting
# your local directories/files.
# Example cmd line:
# docker build --build-arg USERID=$UID --build-arg GROUPID=$UID --build-arg USERNAME=memo .
ARG USERNAME=gitlab-ci
ARG USERID=100000
ARG GROUPID=10043
RUN groupadd -r -g ${USERID} ${USERNAME} \
  && useradd --no-log-init -r -m -g ${USERNAME} -u ${GROUPID} ${USERNAME}

# To use 'sudo', uncomment the following two lines(and update password if needed)
#RUN usermod -aG wheel ${USERNAME}
#RUN echo "${USERNAME}:gitlabpass" | chpasswd

# Create /epics directory in the image so that we can test
# without mounting an external dir
RUN mkdir /epics && chown ${USERNAME}:${USERNAME} /epics

USER ${USERNAME}
WORKDIR /home/${USERNAME}

# Setup git
# Set color.ui to avoid google git repo to prompt for it
RUN git config --global user.name ${USERNAME} \
  && git config --global user.email ${USERNAME}@localhost.localdomain \
  && git config --global color.ui auto \
  && git config --global --add safe.directory "*"
